import React, {Component} from 'react';
import { ScrollView } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import computeProps from '../../utils/computeProps';
import SCALE from '../../utils/scale';
import COLOR from '../../utils/color';
import _ from 'lodash';

export default class Content extends Component {

    propTypes: {
        style           : React.PropTypes.object,
        hasHeader       : React.PropTypes.boolean,
        hasFooter       : React.PropTypes.boolean,
        scrollEnabled   : React.PropTypes.boolean,
        transparent   : React.PropTypes.boolean,
    }
    constructor(props) {
        super(props);
        this.state = {
            scrollEnabled : false,
        },
        this.scrollEnabled = true;
    }
    getInitialStyle() {
        return {
            flexDirection   : 'column',
            backgroundColor : this.props.transparent?COLOR.COLOR_TRANSPARENT:COLOR.COLOR_WHITE,
            height          : SCALE.DEVICE_HEIGHT - ((this.props.hasHeader !== false)?SCALE.RATIO_Y*50:0) - (this.props.hasFooter?SCALE.RATIO_Y*50:0),
            width           : SCALE.DEVICE_WIDTH,
        }
    }

    prepareRootProps() {
        let defaultProps = {
            showsVerticalScrollIndicator    : false,
            style                           : this.getInitialStyle(),
        };
        let props = this.props;
        if(typeof props.scrollEnabled != 'undefined'){
            this.scrollEnabled = props.scrollEnabled;
        }
        delete props.scrollEnabled;
        return computeProps(props, defaultProps);

    }
    renderChildren() {
        return this.props.children;
    }
    render() {
        return(
            <KeyboardAwareScrollView {...this.prepareRootProps()} scrollEnabled={this.state.scrollEnabled} onContentSizeChange={(w,h)=>{ if(this.scrollEnabled)this.setState({scrollEnabled:this.getInitialStyle().height<h})}}>
                {this.renderChildren()}
            </KeyboardAwareScrollView>
        );
    }
}
