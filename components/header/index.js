import Header from './Header';
import HeaderTitle from './HeaderTitle';
import HeaderLeft from './HeaderLeft';
import HeaderRight from './HeaderRight';
module.exports = {
	Header		: Header,
	HeaderTitle	: HeaderTitle,
	HeaderLeft	: HeaderLeft,
	HeaderRight	: HeaderRight,
}
