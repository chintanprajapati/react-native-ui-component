import React, {Component} from 'react';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import computeProps from '../../utils/computeProps';
import SCALE from '../../utils/scale';
import COLOR from '../../utils/color';
import _ from 'lodash';
active=false;
export default class Footer extends Component {

    propTypes: {
        style : React.PropTypes.object,
        data  : React.PropTypes.object,
        active: React.PropTypes.boolean,
    }

    constructor(props) {
        super(props);
        this.state = {}
    }

    getInitialStyle() {
        return {
            flexDirection   : 'column',
            alignItems      : 'center',
            justifyContent  : 'space-around',
            backgroundColor : COLOR.COLOR_TRANSPARENT,
            height          : SCALE.RATIO_Y*50,
            padding         : SCALE.PADDING_5
        }
    }

    getIconStyle(){
        return {
            height          : (SCALE.UNIT<SCALE.RATIO_Y?SCALE.UNIT:SCALE.RATIO_Y)*25,
            width           : (SCALE.UNIT<SCALE.RATIO_Y?SCALE.UNIT:SCALE.RATIO_Y)*25,
        }
    }
    getTitleStyle(){
        return {
            color       : (this.props.data.active || this.props.active || this.state.active)?COLOR.COLOR_PRIMARY:COLOR.COLOR_GRAY,
            fontSize    : SCALE.FONT_SIZE_13,
            fontFamily  : 'Futura',
        }
    }

    prepareRootProps() {
        let defaultProps = {
            style: this.getInitialStyle()
        };
        return computeProps(this.props, defaultProps);

    }

    getNotificationStyle(){
        return {
            view: {
                minWidth: 16*SCALE.RATIO_Y,
                height: 16*SCALE.RATIO_Y,
                borderRadius: 8*SCALE.RATIO_Y,
                backgroundColor: COLOR.COLOR_PRIMARY,
                flexDirection: 'column',
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
                position: 'relative',
                top: -SCALE.RATIO_Y*30,
                right: -SCALE.RATIO_Y*10,
                marginTop: - 14*SCALE.RATIO_Y,
            },
            text: {
                fontSize: 9*SCALE.RATIO_Y,
                fontWeight: '500',
                color: COLOR.COLOR_WHITE,
            }

        }
    }

    getNotification() {
        if(this.props.data.notification){
            return (
                <View style={this.getNotificationStyle().view}>
                    <Text allowFontScaling={false} style={this.getNotificationStyle().text}>{this.props.data.notification}</Text>
                </View>
            )
        }
    }
    renderChildren() {
        let newChildren = [];
        newChildren.push(<Image key='_icon' style={this.getIconStyle()} source={(this.props.data.active || this.props.active || this.state.active)?this.props.data.activeIcon:this.props.data.icon}/>);
        newChildren.push(<Text allowFontScaling={false} key='_title' style={this.getTitleStyle()}>{this.props.data.title}</Text>);
        return newChildren;
    }
    render() {
        return(
            <TouchableOpacity {...this.prepareRootProps()} activeOpacity={0.9} onPressIn={()=>{this.setState({active:true})}} onPressOut={()=>{this.setState({active:false})}}>
                {this.renderChildren()}
                {this.getNotification()}
            </TouchableOpacity>
        );
    }
}
