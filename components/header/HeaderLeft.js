import React, {Component} from 'react';
import { View, Text } from 'react-native';
import computeProps from '../../utils/computeProps';
import SCALE from '../../utils/scale';
import COLOR from '../../utils/color';
import _ from 'lodash';

export default class HeaderLeft extends Component {

    propTypes: {
        style : React.PropTypes.object,
    }

    getInitialStyle() {
        return {
            alignItems          : 'flex-start',
            justifyContent      : 'center',
            width               : SCALE.DEVICE_WIDTH/2,
            height              : SCALE.RATIO_Y*(SCALE.PLATFORM_OS == 'ios'?60:50),
            top                 : SCALE.PLATFORM_OS == 'ios'?SCALE.RATIO_Y*8:0,
            left                : 0,
            paddingHorizontal   : SCALE.PADDING_5,
        }
    }

    prepareRootProps() {
        var defaultProps = {
            style: this.getInitialStyle(),
        };
        return computeProps({},defaultProps);

    }
    renderChildren() {
        return this.props.children;
    }
    render() {
        return(
            <View {...this.prepareRootProps()} >
                {this.renderChildren()}
            </View>
        );
    }
}
