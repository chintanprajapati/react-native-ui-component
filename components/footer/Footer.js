import React, {Component} from 'react';
import { View } from 'react-native';
import computeProps from '../../utils/computeProps';
import SCALE from '../../utils/scale';
import COLOR from '../../utils/color';
import FooterTab from './FooterTab';
import _ from 'lodash';

export default class Footer extends Component {

    propTypes: {
        style       : React.PropTypes.object,
        footerTab   : React.PropTypes.array,
    }

    getInitialStyle() {
        return {
            flexDirection   : 'row',
            alignItems      : 'center',
            justifyContent  : 'space-around',
            backgroundColor : COLOR.COLOR_WHITE,
            height          : SCALE.RATIO_Y*50,
            width           : SCALE.DEVICE_WIDTH,
            shadowColor     : COLOR.COLOR_GRAY,
            shadowOffset    : {
                                width:0,
                                height: 1,
                              },
            shadowOpacity   : 0.2,
            borderColor     : COLOR.COLOR_WHITE,
        }
    }

    prepareRootProps() {
        var defaultProps = {
            style: this.getInitialStyle()
        };
        return computeProps(this.props, defaultProps);

    }
    renderChildren() {
        if(this.props.footerTab){
            let newChildren = [],
            length = this.props.footerTab.length;
            this.props.footerTab.map((child, i) => {
                if(child.onPress){
                    let onPress = child.onPress;
                    // delete child.onPress;
                    newChildren.push(<FooterTab data={child} onPress={onPress} style={{width:SCALE.DEVICE_WIDTH/length}} key={'_footerTab'+i} />)
                }else{
                    newChildren.push(<FooterTab data={child} style={{width:SCALE.DEVICE_WIDTH/length}} key={'_footerTab'+i} />)
                }

            });
            return newChildren;
        }
        return this.props.children;
    }
    render() {
        return(
            <View {...this.prepareRootProps()} >
                {this.renderChildren()}
            </View>
        );
    }
}
